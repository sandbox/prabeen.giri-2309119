<?php
/**
 * @file
 * Admin related callback and help functions.
 */

/**
 * Access Callback for admin page.
 */
function role_based_homepage_admin_form() {
  global $user;

  role_based_homepage_get_homepage($user);

  $form = array();
  $form['role_based_homepage'] = array(
      '#type' => 'item',
      '#description' => t("Role with lighter weight will have higher precedence if user has multiple roles. Role with empty homepage does not play any role."),
      '#tree' => TRUE,
      '#theme' => 'role_based_homepage_admin_form',
      '#title' => t('User will be redirected to the path specifed below when user logs in and accesses front page or base url'),
  );

  $delta = 10;

  // This will hold the maximum weight value that already exists.
  $maximum_sorted_weight_value = $delta * -1;

  // Create the table grid.
  foreach (_role_based_homepage_roles() as $role_id => $role_name) {
    // Get the exiting value from the variable table if it exists.
    $data = variable_get('role_based_homepage_role_' . $role_id);
    // Get the weight if its already saved, else give own weight value
    // based on the last minimum value of saved data.
    // This won't create conflict if user creates two homepages but
    // does not sort the order, which will save the same weight.
    if ($data) {
      $weight = $data['weight'];
      if ($data['weight'] > $maximum_sorted_weight_value) {
        $maximum_sorted_weight_value = $weight;
      }
    }
    else {
      $weight = $maximum_sorted_weight_value  = $maximum_sorted_weight_value + 1;
    }

    // Sort by weight, else role name.
    if (isset($data['weight'])) {
      $form['role_based_homepage']['role_' . $role_id] = array(
          '#weight' => $weight,
      );
    }

    $form['role_based_homepage']['role_' . $role_id]['role_name'] = array(
        '#markup' => check_plain($role_name),
    );

    $form['role_based_homepage']['role_' . $role_id]['role_homepage'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($data['homepage']) ? $data['homepage'] : "",
        '#theme' => 'role_based_homepage_path_textfield',
    );
    $form['role_based_homepage']['role_' . $role_id]['weight'] = array(
        '#type' => 'weight',
        '#delta' => $delta,
        '#attributes' => array('class' => array('role-based-homepage-role-weight')),
        '#default_value' => $weight,
    );
  }

  // Other settings.
  $form['role_based_homepage_other_settings'] = array(
      '#title' => t('Other Settings'),
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
  );
  $form['role_based_homepage_other_settings']['role_based_homepage_preserve_destination_parameter'] = array(
      '#type' => 'checkbox',
      '#title' => t('Preserve the destination parameter when user logs in.'),
      '#description' => t("The 'destination' GET parameter will have priority over the settings of this module."),
      '#default_value' => variable_get('role_based_homepage_preserve_destination_parameter'),
  );

  // Default content settings.
  $form['role_based_homepage_other_settings']['role_based_homepage_display_default_content'] = array(
      '#type' => 'checkbox',
      '#title' => t('Deliver default content when user does not have valid/empty homepage.'),
      '#description' => t("This will deliver the content as provided below regardless which page they gets redirected to if user does not have valid homepage. "),
      '#default_value' => variable_get('role_based_homepage_display_default_content'),
  );

  $form['role_based_homepage_other_settings']['role_based_homepage_default_content'] = array(
      '#type' => 'textarea',
      '#title' => t('Default HTML content'),
      '#default_value' => variable_get('role_based_homepage_default_content'),
      '#rows' => 7,
      '#states' => array(
          'visible' => array(
              'input[name="role_based_homepage_display_default_content"]' => array('checked' => TRUE),
          ),
      ),
  );

  // Log into the watchdog option.
  $form['role_based_homepage_other_settings']['role_based_homepage_log_into_watchdog'] = array(
      '#type' => 'checkbox',
      '#title' => t("Log into watchdog user have valid/empty homepage. If checked, this will log only if 'Deliver default content' option is unchecked."),
      '#description' => t("Log into watchdog with 'alert' severity if any user does not valid homepage."),
      '#default_value' => variable_get('role_based_homepage_log_into_watchdog', TRUE),
  );

  $form['#submit'] = array('role_based_homepage_admin_form_submit');
  return system_settings_form($form);
}

/**
 * Theme callback role_based_homepage_path_textfield.
 */
function theme_role_based_homepage_path_textfield($variables) {
  global $base_url;

  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  element_set_attributes($element, array(
  'id', 'name', 'value', 'size', 'maxlength',
  ));
  _form_set_class($element, array('form-text'));

  $extra = '';
  if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#attributes']['id'] . '-autocomplete';
    $attributes['value'] = url($element['#autocomplete_path'], array('absolute' => TRUE));
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }
  $output = '<span>' . $base_url . '/ </span><input' . drupal_attributes($element['#attributes']) . ' />';

  return $output . $extra;
}
/**
 * Validate Callback for the admin form role_based_homepage_admin_form().
 */
function role_based_homepage_admin_form_validate(&$form, &$form_state) {
  global $base_url;
  $data = $form_state['values']['role_based_homepage'];
  foreach ($data as $key => $details) {
    if (!empty($details['role_homepage']) && !$menu = menu_get_item($details['role_homepage'])) {
      form_set_error("role_based_homepage][$key][role_homepage", t('Invalid drupal path <em>@path</em>', array('@path' => $base_url . "/" . $details['role_homepage'])));
    }
    elseif ($menu) {
      // @todo Try to check if the specified role as access to its respective
      // homepage as specified.
    }
  }
}

/**
 * Submit handler for role_based_homepage_admin_form().
 */
function role_based_homepage_admin_form_submit(&$form, &$form_state) {

  $data = $form_state['values']['role_based_homepage'];
  foreach ($data as $key => $details) {
    if (!empty($details['role_homepage'])) {
      variable_set('role_based_homepage_' . $key, array('homepage' => $details['role_homepage'], 'weight' => $details['weight']));
    }
    else {
      variable_del('role_based_homepage_' . $key);
    }
  }
  // Stop settings form default handler from saving into variable
  // table with key 'role_based_homepage'. Its due to the form
  // structure we have in the settings form which cannot be avoided.
  unset($form_state['values']['role_based_homepage']);
}

/**
 * Theme callback for role_based_homepage_admin_form.
 */
function theme_role_based_homepage_admin_form($variables) {
  $form = $variables['form'];
  $header = array(t('Role Name'), t("Homepage"),  t('Weight'));
  $rows = array();
  $childrens = element_children($form);
  foreach ($childrens as $key) {
    $row = array();
    $element = $form[$key];
    $row[] = drupal_render($element['role_name']);
    $row[] = drupal_render($element['role_homepage']);
    $row[] = drupal_render($element['weight']);
    $rows[] = array('data' => $row, 'class' => array('draggable'));
  }

  drupal_add_tabledrag('role-based-homepage-admin-table', 'order', 'sibling', 'role-based-homepage-role-weight');
  return theme('table', array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array(
          'id' => 'role-based-homepage-admin-table',
      ),
  ));
}

