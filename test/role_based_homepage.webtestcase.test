<?php
/**
 * @file
 * WebTest for Role Based Homepage.
 */

/**
 * Tests the functionality Role based drupal homepage.
 */
class RoleBasedHomepageTestCase extends DrupalWebTestCase {
  protected $privilegedUser;

  /**
   * Set the information to run on the Suite.
   */
  public static function getInfo() {
    // Note: getInfo() strings are not translated with t().
    return array(
      'name' => 'Role Based Homepage',
      'description' => 'Ensures that user redirect to correct homepage after login, use of one time link, being redirect to the home',
      'group' => 'Role Based Homepage',
    );
  }

  /**
   * Set up all the required modules.
   */
  public function setUp() {
    // Enable any modules required for the test. This should be an array of
    // module names.
    parent::setUp(array('role_based_homepage'));
  }

  /**
   * Tests if the user is redirect to correct homepage after login.
   */
  public function testLoginToHomepage() {
    // Set up Data first.
    variable_set('role_based_homepage_test_user_role', 'user/test');

    // Test.
    $homepage = variable_get('role_based_homepage_test_user_role');
    $user = $this->drupalCreateUser(array('access content'));
    $this->drupalLogin($user);
    $this->assertEqual($this->getUrl(), $homepage, "User redirected to the correct page after login.");
  }

  /**
   * Test if homepage redirect when visited the baseurl.
   *
   * This should work only if user is logged in.
   */
  public function testBaseURLRedirectedToHomePage() {
    global $base_url;
    variable_set('role_based_homepage_test_user_role', 'user/test');
    $user = $this->drupalCreateUser(array('access content'));
    $this->drupalGet($base_url);
    $this->drupalLogin($user);
    $homepage = variable_get('role_based_homepage_test_user_role');
    $this->assertEqual($this->getUrl(), $homepage, "User redirected to the correct page when visited the BASEURL.");
  }

  /**
   * Test if roles and homepage is being added on the admin.
   */
  public function testAdminAddRoleAndHomepageURL() {
    $user = $this->drupalCreateUser(array('administer site configuration'));
    $this->drupalLogin($user);
    $edit = array(
      'role_based_homepage_test_user_role' => 'user/test',
      'role_based_homepage_test_user_role1' => 'user/test1',
      'role_based_homepage_test_user_role2' => 'user/test2',
    );
    $this->drupalPost('admin/config/people/role-based-homepage', $edit, t('Save configuration'));

    $this->assertEqual(variable_get('role_based_homepage_test_user_role'), 'user/test');
    $this->assertEqual(variable_get('role_based_homepage_test_user_role1'), 'user/test1');
  }

  /**
   * Test destination parameter setting.
   */
  public function testIfDestinationIsBeingPreserved() {

  }
}
