<?php
/**
 * @file
 * This file contains the hooks that can be used by other modules.
 */

/**
 * This hook can be used to alter the final redirect page.
 *
 * If redirect for some role is defined by some custom logics, then
 * this hook can be implemented and alter the edit value.
 *
 * This will have precedence over the setting used in the
 * admin. But not over the destination parameter in the GET
 * if its checked in the admin.
 *
 * @param stdClass $user
 *   Drupal User object.
 * @param array $edit
 *   Associative array with 'homepage' key.
 */
function hook_role_based_homepage_redirect_alter($user, &$edit) {
  if (in_array('test role', array_values($account->roles)) && $account->uid == 1) {
    $edit['homepage'] = 'page/awesome';
  }
}
